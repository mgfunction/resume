---
geometry:
  - margin=0.45in
hyperrefoptions:
  - pdfpagemode=UseNone
  - pdfpagetransition=Fade
  - pdfstartview=Fit
  - pdftoolbar=false
  - pdfmenubar=false
  - pdfcenterwindow
  - pdffitwindow
  - pdfdisplaydoctitle
pagestyle: empty
fontsize: 10pt
---

\thispagestyle{empty}

\begin{center}

{\Huge Caleb Darling}

\href{https://www.openstreetmap.org/relation/305467}{Manchester, NH 03103} $\bullet$
\href{tel:+1603-440-4001}{(603) 440-4001} $\bullet$ 
\href{mailto:calebd@rling.me}{calebd@rling.me} $\bullet$ 
\href{http://calebdar.link}{calebdar.link (Linkedin)} $\bullet$ 
\href{http://gitlab.com/mgfunction/resume}{gitlab.com/mgfunction/resume}

\end{center}
