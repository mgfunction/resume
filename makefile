all: clean resume.pdf
	rm -f resume.md

resume.%: %header.md resume.md
	pandoc -s $< resume.md -o $@

resume.md: content.md
	cp $< resume.md
	sed -i 's/<+>/`\\hfill`{=tex} `<br>`{=html}/gi' resume.md

clean: content.md
	pandoc $< -o $< -t "markdown-multiline_tables-grid_tables"
	sed -i 's/\\<+\\>/<+>/gi' $<
