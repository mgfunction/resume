---
title: Caleb Darling
subtitle: '[Amherst, NH 03031](https://www.openstreetmap.org/relation/324602) $\bullet$ [(603) 440-4001](tel:+1603-440-4001) $\bullet$ <calebd@rling.me> $\bullet$ [calebdar.link](http://calebdar.link) $\bullet$ [gitlab.com/mgfunction/resume](http://gitlab.com/mgfunction/resume)'
---
