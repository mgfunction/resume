# Summary

Innovative and creative problem solver eager to break into the Data
Analyst space. Mathematics graduate with over two years of professional
working experience looking to bring my extensive familiarity with
analysis tools, mathematics, and programming languages into my career
trajectory.

# Skills

  -------- ----- ------- ------- --------- ------------ --------- ------------
  Python     R    Linux    SQL    Minitab      HTML       C/C++       Git
  Pandas    SAS   Bash    Excel    Latex    Javascript   Haskell   Salesforce
  -------- ----- ------- ------- --------- ------------ --------- ------------

# Relevant Experience

| **Check Print Automation <+> Spring 2022**
| *Southern New Hampshire University <+> Manchester, NH*

-   Designed and wrote a program in Python using Pandas to transform
    user and Salesforce check data into a format that could be fed into
    the check printer to replace a tedious manual process
-   Collaborated with head of Dev Ops to create a service running on the
    print server to run the script when files are added to the input
    folder
-   Presented project to key stakeholders to receive feedback and
    implemented all suggested changes

| **Senior Seminar I and II <+> Fall 2019**
| *University of Massachusetts Lowell <+> Lowell, MA*

-   Demonstrated creativity by coming up with a unique problem based
    around number theory and linear algebra
-   Utilized research skills and mathematical analysis to solve problem
-   Precisely communicated problem and its solutions via written form
    and oral presentation

| **Design and Analysis of Experiments <+> Fall 2018**
| *University of Massachusetts Lowell <+> Lowell, MA*

-   Designed 7 factor fractional factorial experiment, using Minitab,
    for paper airplane flight
-   Led group in designing and building a launcher to minimize external
    factors and isolate variables
-   Conducted experiment with group, performed analysis in Minitab, and
    presented results in class

# Professional Experience

| **Admissions Processing Coordinator <+> February 2021 to Present**
| *Southern New Hampshire University <+> Manchester, NH*

-   Exemplified creativity by automating the creation of a tedious email
    from Salesforce data
-   Demonstrated knowledge of analysis tools by using Pandas to automate
    the creation of csv files of data for physical checks
-   Embody ambition by taking on special projects as well as roles
    outside of the department

| **Housing Intake Support Specialist <+> August 2020 to January
  2021**
| *Southern New Hampshire Services <+> Manchester, NH*

-   Displayed initiative to automate processes by scripting the filling
    out of invoice forms
-   Used communication skills to guide clients through difficult housing
    relief process
-   Showcased ability to work remotely and independently

# Education

| **Bachelors of Science in Mathematics,** Probability and Statistics
  Concentration <+> **September 2015 to December 2019**
| *University of Massachusetts Lowell <+> Lowell, MA*
| 3.40 GPA
